# CatuSell

2020年 - 資料庫系統專題

## 3-tier Application

![Basic Architecture](3TBasic.png)

### Overview

![Project Architecture](projArch.png)

### Application

A Web App, served by Nginx

### Server

Using Flask to handle requests, written in python.

### DB Server

Using MySQL with phpMyAdmin.
