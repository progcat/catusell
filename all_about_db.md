
## ports

mysql: localhost:3306
phpMyAdmin: localhost:4000

## users

```
user@%
passwd: goodpasswd

root@localhost
passwd: 123456
```

## Get container ip
```
sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id
```

## direct access sql_server with root
```
sudo docker exec -it sql_server mysql -u root -p
```

## setup mysql

1. create user
```
CREATE USER 'user'@'%' IDENTIFIED WITH mysql_native_password BY 'goodpassword';
```

2. grant permission
```
GRANT ALL ON *.* TO 'user'@'%';
```

3. update permission
```
FLUSH PRIVILEGES;
```
