
## User

| HTTP Method | URI | Action |
|-|-|-|
| POST | /api/user/register | Register |
| POST | /api/user/login | Login |
| GET | /api/user/info?token=\<token> | Get current user info |

**Register**
```
{"username", "pw", "real_name", "phone_no"}
Res: 200 if OK / 500 {"reason"} if fail
```

**Login**
```
Req: {"username", "pw"}
Res: 200 if OK / 500 {"reason"} if fail
```

**Get user info**
```
Req: None
Res: 200 if OK {"username", "phone_no", "real_name"} / 500 if fail
```

## Item

| HTTP Method | URI | Action |
|-|-|-|
| POST | /api/item?token=\<token> | Put item on the market |
| PUT | /api/item?token=\<token> | Update item |
| GET | /api/item/list | Get item list |
| GET | /api/item/rand | Get random item |
| GET | /api/item | Get specific item |
| DELETE | /api/item?token=\<token> | Off seft |

**Put Item on the market**
```
Req: {"name", "state", "price", "desc"}
Res: 200 if OK / 500 {"reason"} if fail
```

**Update item**
```
Req: {"item_id", "name", "state", "price", "desc"}
Res: 200 if OK / 500 {"reason"} if fail
```

**Get item list**
```
Req: None
Res: 200 [{"item_id", "name"}] if OK / 500 {"reason"} if fail
```

**Get random item**
```
Req: None
Res: 200 {"item_id", "name", "state", "price", "desc"} if OK / 500 if fail
```

**Get specific item**
```
Req: {"item_id"}
Res: 200 {"item_id", "name", "state", "price", "desc"} if OK / 500 if fail
```

**Delete item**
```
Req: {"item_id"}
Res: 200 if OK / 500 if fail
```

## Transaction

| HTTP Method | URI | Action |
|-|-|-|
| POST | /api/transac?token=\<token> | Place order
| GET | /api/transac/list?token=\<token> | Get all transaction

**Place order**
```
Req: {"item_id", "qty", "address"}
Res: 200 if OK / 500 if fail
```

**Get all orders**
```
Req: None
Res: 200 [{"order_id", "name", "item_id", "qty", "address", "price"}] if OK
```
