from flask import Flask, jsonify, request, Response, redirect, render_template, flash
from flask_login import LoginManager, UserMixin, login_user, logout_user, current_user, login_required
from flask_bootstrap import Bootstrap
import logging
import os
import json
import db

from forms import LoginForm, RegisterForm, ItemForm, TransactionForm, ShelfForm, TransacRecordForm

DEBUG_MODE = True

app = Flask(__name__,
    template_folder='templetes')
app.config['SECRET_KEY'] = 'THIS IS A SECRET!!!'

Bootstrap(app)

auth = LoginManager(app)
auth.login_view = 'login_page'
auth.login_message = '請先登入！'
auth.login_message_category = 'info'

@app.before_first_request
def initialize_everything():
    db.init('172.20.0.2:3306')

class SiteUser(UserMixin):
    def __init__(self, db_user, active=True):
        super().__init__()
        self.id = db_user.id
        self.username = db_user.username
        self.real_name = db_user.real_name
        self.phone_no = db_user.phone_no
        self.is_admin = db_user.is_admin
        self.join_at = db_user.join_at
        self.active = active
    
    def get_id(self):
        return self.id
    
    @property
    def is_active(self):
        return self.active

@auth.user_loader
def get_user(uid):
    db_user = db.get_user(uid=uid)
    return SiteUser(db_user)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('你已登出')
    return redirect('/login')

## Page feeder
# main page
@app.route('/', methods=['GET'])
@login_required
def main_page():
    item = db.get_rand_item(db.get_user(current_user.username))
    return render_template('main.html', item_id=item.id, item_name=item.name, price=item.price, desc=item.item_desc)
    
@app.route('/login', methods=['GET', 'POST'])
def login_page():
    form = LoginForm()
    if form.validate_on_submit():
        #submit pressed
        username = form.username.data
        pw = form.pw.data
        # if user exist
        if db.user_exist(username):
            user = db.get_user(username=username)
            if pw == user.passwd and not user.blocked:
                login_user(SiteUser(user))
                return redirect('/' or request.args.get('next'))
            if user.blocked:
                flash('你登入的帳號已被封鎖!', 'info')
                return redirect('/login')
        # user not found & wrong pw goes here
        flash('帳號或密碼錯誤', 'error')
    else:
        if form.username.data and form.pw.data:
            if form.username.validate(form) == False:
                flash('無效的帳號！', 'error')
            elif form.pw.validate(form) == False:
                flash('無效的密碼！', 'error')
            else:
                flash('發生了未知錯誤', 'error')

    return render_template('login.html', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register_page():
    form = RegisterForm()
    if form.validate_on_submit():
        if form.pw_confirm.data != form.pw.data:
            flash('密碼不一致！', 'error')
        elif db.user_exist(form.username.data):
            flash('用戶名稱已被使用', 'error')
        else:
            #Create user
            db.User(blocked=False,
            phone_no=form.teleno.data,
            passwd=form.pw.data,
            real_name=form.realname.data,
            username=form.username.data,
            is_admin=False)
            flash('{}, 歡迎加入仙人賣！'.format(form.username.data))
            login_user(SiteUser(db.get_user(username=form.username.data)))
            return redirect('/') # login and redirect to mainpage
    else:
        if form.username.validate(form) == False and form.username.data:
            flash('無效的用戶名稱！', 'error')
            form.username.data = ''

        if form.realname.validate(form) == False and form.realname.data:
            flash('無效的真實姓名！', 'error')
            form.realname.data = ''

        if form.teleno.validate(form) == False and form.teleno.data:
            flash('無效的電話號碼！', 'error')
            form.teleno.data = ''

        if form.pw.validate(form) == False and form.pw.data:
            flash('無效的密碼！', 'error')
            form.pw.data = ''

    return render_template('register.html', form=form)

@app.route('/userinfo', methods=['GET'])
@login_required
def userinfo_page():
    return render_template('userinfo.html', user=current_user)

@app.route('/additem', methods=['GET', 'POST'])
@login_required
def add_item():
    form = ItemForm()
    if form.validate_on_submit():
        db.Item(name=form.name.data,
        seller=db.get_user(uid=current_user.id),
        state=int(form.state.data),
        price=form.price.data,
        item_desc=form.desc.data)
        flash('上架成功!')
        return redirect('/shelf')
    else:
        if form.name.validate(form) == False and form.name.data:
            flash('無效的商品名稱', 'error')
        if form.state.validate(form) == False and form.state.data:
            flash('無效的商品狀態', 'error')
            form.state.data = ''
        if form.price.validate(form) == False and form.price.data:
            flash('無效的價格', 'error')
            form.price.data = ''
        if form.desc.validate(form) == False and form.desc.data:
            flash('無效的商品描述', 'error')
            form.name.data = ''
    return render_template('additem.html', form=form)

@app.route('/buyitem', methods=['GET', 'POST'])
@login_required
def buyitem_page():
    form = TransactionForm()
    if form.validate_on_submit():
        item = list(db.Item.select(db.Item.q.id == form.item_id.data))[0]
        # not avaiable
        if item.state != 1:
            flash('商品暫時無法購買!', 'error')
            return redirect('/')
        qty = form.qty.data
        total_price = item.price * qty
        address = form.address.data

        db.Transaction(state=1,
        buyer=db.get_user(uid=current_user.id),
        seller=item.seller,
        address=address,
        qty=qty,
        item=item,
        price=total_price)
        flash('購買成功!')
        return redirect('/')
    else:
        if form.item_id.validate(form) and form.item_id:
            flash('無效商品編號')
        if form.address.validate(form) and form.address:
            flash('無效送貨地址')
        
    return render_template('buyitem.html', form=form)

@app.route('/shelf', methods=['GET', 'POST'])
@login_required
def shelf_page():
    items = db.Item.select(db.Item.q.seller == db.get_user(uid=current_user.id))
    form = ShelfForm()
    if form.validate_on_submit():
        item = db.Item.get(int(form.item_id.data))
        if form.del_btn.data:
            # delete item
            db.Item.delete(item.id)
            flash('已刪除 {}'.format(item.name))
            return redirect('/shelf')
        else:
            # edit item
            return redirect('/updateitem/{}'.format(form.item_id.data))
    return render_template('shelf.html', items=items, form=form)

@app.route('/updateitem/<id>', methods=['GET', 'POST'])
@login_required
def updateitem_page(id):
    form = ItemForm()
    item = db.Item.get(id)
    if form.validate_on_submit():
        item.name = form.name.data
        item.state = int(form.state.data)
        item.price = form.price.data
        item.item_desc = form.desc.data
        return redirect('/shelf')
    form.name.data = item.name
    form.state.data = item.state
    form.price.data = item.price
    form.desc.data = item.item_desc
    return render_template('updateitem.html', form=form, id=id)

@app.route('/orderlist', methods=['GET', 'POST'])
@login_required
def orderlist_page():
    form = TransacRecordForm()
    if form.validate_on_submit():
        # tag transsaction
        transac = db.Transaction.get(int(form.transac_id.data))
        transac.state = 0
        flash('{}已完成交易'.format(transac.item.name))
        return redirect('/orderlist')
    trans = list(db.Transaction.select())
    return render_template('orderlist.html', form=form, transacs=trans)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000, debug=False)
