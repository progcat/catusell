import sqlobject
from tables import User, Item, Transaction
import random

def init(server_address: str = 'localhost:3306'):
    sqlobject.sqlhub.processConnection = sqlobject.connectionForURI('mysql://user:goodpasswd@{}/database?debug=1&charset=utf8'.format(server_address))
    # Create tables
    print('Creating tables...')
    User.createTable(ifNotExists=True)
    Item.createTable(ifNotExists=True)
    Transaction.createTable(ifNotExists=True)

def user_exist(username):
    if not username:
        raise ValueError('username is empty')
    res = list(User.select(User.q.username == username))
    if len(res):
        return True
    return False

def get_user(username=None, uid=None):        
    if username:
        return list(User.select(User.q.username == username))[0]
    if uid:
        return list(User.select(User.q.id == uid))[0]
    raise ValueError('Call with no keys')

def get_rand_item(user, n=1):
    items = list(Item.select(Item.q.seller != user))
    if n == 1:
        return random.choice(items)
    return random.sample(items, n)