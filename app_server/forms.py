from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, HiddenField, PasswordField, SubmitField, FloatField, SelectField
from wtforms.fields.html5 import IntegerField
from wtforms.widgets.html5 import NumberInput
import wtforms.validators as valid

class LoginForm(FlaskForm):
    username = StringField('帳號', validators=[valid.DataRequired(), valid.Regexp('^[1-zA-Z0-9]+$')])
    pw = PasswordField('密碼', validators=[valid.DataRequired(), valid.Length(6, 32), valid.Regexp('^[\w\d]+$')])
    submit = SubmitField('登入', render_kw={"class": 'btn'})

class RegisterForm(FlaskForm):
    username = StringField('帳號',
    validators=[valid.DataRequired(), valid.Length(4, 32), valid.Regexp("^[\w\d]+$")],
    render_kw={"placeholder": '4-32個英數組合'}
    )
    realname = StringField('真實姓名',
    validators=[valid.DataRequired(), valid.Length(3), valid.Regexp("^\w+$")],
    render_kw={"placeholder": '至少3個字'}
    )
    teleno = StringField('電話號碼',
    validators=[valid.DataRequired(), valid.Length(10, 10), valid.Regexp("^\d+$")],
    render_kw={"placeholder": '10個數字'}
    )
    pw = PasswordField('密碼',
    validators=[valid.DataRequired(), valid.Length(6,32), valid.Regexp("^[\w\d]+$")],
    render_kw={"placeholder": '6-32個英數字'}
    )
    pw_confirm = PasswordField('確認密碼',
    validators=[valid.DataRequired(), valid.Length(6,32), valid.Regexp("^[\w\d]+$")],
    render_kw={"placeholder": '再輸入一次密碼'}
    )
    submit = SubmitField('送出', render_kw={"class": 'btn'})

class ItemForm(FlaskForm):
    name = StringField('商品名稱',
    validators=[valid.DataRequired(), valid.Length(1, 32)],
    render_kw={"placegolder": '最多32個字'}
    )
    state = SelectField('商品狀態',
    validators=[valid.DataRequired()], choices=[(0, '下架'), (1, '上架中')], validate_choice=False)

    price = FloatField('價格',
    validators=[valid.DataRequired()])

    desc = TextAreaField('商品描述')

    submit = SubmitField('送出', render_kw={"class": 'btn'})

class TransactionForm(FlaskForm):
    item_id = HiddenField(id='item_id', validators=[valid.DataRequired(), valid.Regexp('^\d+$')])
    qty = IntegerField('數量', widget=NumberInput(min=1, step=1), default=1, id="qty_input", validators=[])
    address = StringField('送貨地址', validators=[valid.DataRequired(), valid.Length(1)])
    submit = SubmitField('下訂單', render_kw={"class": 'btn float-right'})

class TransacRecordForm(FlaskForm):
    transac_id = HiddenField(id='transac_id', validators=[valid.DataRequired(), valid.Regexp('^\d+$')])
    finish = SubmitField('完成', render_kw={"class": 'btn float-right'})

class ShelfForm(FlaskForm):
    item_id = HiddenField(id='item_id', validators=[valid.DataRequired(), valid.Regexp('^\d+$')])
    edit_btn = SubmitField('編輯', render_kw={"class": 'btn float-right'})
    del_btn = SubmitField('刪除', render_kw={"class": 'btn float-right'})
