import sqlobject as sql

class User(sql.SQLObject):
    blocked = sql.BoolCol(default=False, notNone=True)
    phone_no = sql.StringCol(notNone=True, length=10)
    passwd = sql.StringCol(notNone=True)
    real_name = sql.StringCol(notNone=True)
    username = sql.StringCol(notNone=True, unique=True, length=32)
    is_admin = sql.BoolCol(default=False, notNone=True)
    join_at = sql.DateTimeCol(default=sql.DateTimeCol.now, notNone=True)

class Item(sql.SQLObject):
    name = sql.StringCol(notNone=True, length=16)
    seller = sql.ForeignKey('User', notNone=True)
    state = sql.IntCol(default=0, length=1, notNone=True)
    price = sql.CurrencyCol(default=0, notNone=True)
    item_desc = sql.StringCol()
    create_at = sql.DateTimeCol(default=sql.DateTimeCol.now, notNone=True)

class Transaction(sql.SQLObject):
    state = sql.IntCol(default=0, length=1, notNone=True)
    buyer = sql.ForeignKey('User', notNone=True)
    seller = sql.ForeignKey('User', notNone=True)
    address = sql.StringCol(length=104, notNone=True)
    qty = sql.IntCol(default=0, length=2, notNone=True)
    item = sql.ForeignKey('Item', notNone=True)
    price = sql.FloatCol(default=0.00, notNone=True)
    create_at = sql.DateTimeCol(default=sql.DateTimeCol.now, notNone=True)

if __name__ == '__main__':
    sql.sqlhub.processConnection = sql.connectionForURI('mysql://user:goodpasswd@localhost:3306/database?debug=1')
    with open('schema_gen.sql', 'w') as fp:
        sql_str = User.createTableSQL()
        fp.write(''.join(sql_str[0]))
        fp.write('\n')

        sql_str = Item.createTableSQL()
        fp.write(''.join(sql_str[0]))
        fp.write('\n')

        sql_str = Transaction.createTableSQL()
        fp.write(''.join(sql_str[0]))
        fp.write('\n')
